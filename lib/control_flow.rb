# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char { |letter| str.delete!(letter) if letter != letter.upcase }
  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  num = str.length / 2
  return str[num] if str.length.odd?
  str[(num - 1)..num]
end

# Return the number of vowels in a string.
def num_vowels(str)
  ans = 0
  str.each_char { |letter| ans += 1 if 'aeiouAEIOU'.include?(letter) }
  ans
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  ans = 1
  idx = num
  num.times do
    ans *= idx
    idx -= 1
  end
  ans
end

# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = '')
  ans = ''
  arr.each_with_index do |letter, index|
    ans += letter + separator if index != arr.length - 1
    ans += letter if index == arr.length - 1
  end
  ans
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  counter = 0
  ans = ''
  str.downcase!
  str.each_char do |letter|
    ans += letter if counter.even?
    ans += letter.upcase if counter.odd?
    counter += 1
  end
  ans
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  ans = []
  str.split(' ').each do |word|
    ans << word.reverse if word.length >= 5
    ans << word if word.length < 5
  end
  ans.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  ans = []
  counter = 1
  n.times do
    if (counter % 15).zero?
      ans << 'fizzbuzz'
    elsif (counter % 5).zero?
      ans << 'buzz'
    elsif (counter % 3).zero?
      ans << 'fizz'
    else
      ans << counter
    end
    counter += 1
  end
  ans
end

# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  ans = []
  num = arr.length
  num.times do
    ans << arr[num - 1]
    num -= 1
  end
  ans
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  idx = num - 1
  while idx > 1
    return false if (num % idx).zero?
    idx -= 1
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  i = num
  ans = []
  while i > 0
    ans << i if (num % i).zero?
    i -= 1
  end
  ans.sort
end

# Write a method that returns a sorted array of the prime factors of its
# argument.
def prime_factors(num)
  fctrs = factors(num)
  ans = []
  fctrs.each { |nums| ans << nums if prime?(nums) }
  ans
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end

# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odds = []
  evens = []
  arr.each do |num|
    odds << num if (num % 2).odd?
    evens << num if (num % 2).even?
  end
  return odds[0] if odds.length == 1
  evens[0]
end
